DROP DATABASE IF EXISTS Proyecto_Mi_Tienda;
CREATE DATABASE if not exists Proyecto_Mi_Tienda;

USE Proyecto_Mi_Tienda;

CREATE TABLE if not exists clientes
(
id VARCHAR(15) PRIMARY KEY,
nombre VARCHAR(50) NOT NULL,
direccion VARCHAR(100),
telefono VARCHAR(10)
)engine = InnoDB;

CREATE TABLE if not exists articulos
(
id INTEGER (11) PRIMARY KEY AUTO_INCREMENT,
nombre VARCHAR(100) UNIQUE NOT NULL,
medida VARCHAR(15) NOT NULL,
stock INTEGER DEFAULT 0
)engine = InnoDB;


CREATE TABLE if not exists usuarios
(
id VARCHAR(20) PRIMARY KEY,
nombre VARCHAR(50) NOT NULL,
contrasena VARCHAR(32) NOT NULL,
rol VARCHAR(20)
)engine = InnoDB;

CREATE TABLE if not exists ventas
(
id INTEGER(11) PRIMARY KEY AUTO_INCREMENT,
id_usuario VARCHAR(10),
valor_total DECIMAL(10,2),
estado VARCHAR(30),
fecha DATE NOT NULL,
FOREIGN KEY (id_usuario) REFERENCES usuarios(id)
)engine = InnoDB;


CREATE TABLE if not exists categorias
(
id INTEGER(11) PRIMARY KEY AUTO_INCREMENT,
nombre VARCHAR(50) UNIQUE NOT NULL
)engine = InnoDB;

CREATE TABLE if not exists productos
(
id INTEGER (11) PRIMARY KEY AUTO_INCREMENT,
codigo VARCHAR(15) UNIQUE NOT NULL,
nombre VARCHAR(100) UNIQUE NOT NULL,
precio DECIMAL(10,2) NOT NULL,
id_categoria INTEGER(11) NOT NULL,
FOREIGN KEY (id_categoria) REFERENCES categorias(id)
);

CREATE TABLE if not exists abonos
(
id INTEGER(11) PRIMARY KEY AUTO_INCREMENT,
id_venta INTEGER(11) NOT NULL,
tipo_pago VARCHAR(30) NOT NULL,
valor_abono DECIMAL(10,2) NOT NULL,
fecha DATE NOT NULL,
FOREIGN KEY (id_venta) REFERENCES ventas(id)
) engine = InnoDB;

CREATE TABLE if not exists ventas_cliente
(
	id_venta INTEGER(11) NOT NULL,
    id_cliente VARCHAR(15) NOT NULL,
    FOREIGN KEY(id_venta) 	REFERENCES ventas(id),
    FOREIGN KEY(id_cliente) REFERENCES clientes(id)
)engine = InnoDB;

CREATE TABLE if not exists detalle_ventas
(
id INTEGER(11) PRIMARY KEY AUTO_INCREMENT,
precio DECIMAL(10,2) NOT NULL,
descuento INTEGER (2),
id_producto INTEGER(11) NOT NULL,
cantidad INTEGER NOT NULL,
id_venta INTEGER(11) NOT NULL,
FOREIGN KEY (id_producto) REFERENCES productos(id),
FOREIGN KEY (id_venta) REFERENCES ventas(id)
)engine = InnoDB;

CREATE TABLE if not exists detalle_productos
(
id INTEGER(11) PRIMARY KEY AUTO_INCREMENT,
cantidad INTEGER(11) NOT NULL,
id_producto INTEGER(11),
id_articulo INTEGER(11),
FOREIGN KEY (id_producto) REFERENCES productos(id),
FOREIGN KEY (id_articulo) REFERENCES articulos(id)
)engine = InnoDB;

CREATE TABLE if not exists proveedores 
(
id INTEGER(11) PRIMARY KEY AUTO_INCREMENT,
razon_social VARCHAR(50) NULL,
tipo_documento VARCHAR(15) NULL,
num_documento VARCHAR(15) UNIQUE NULL,
telefono VARCHAR(10) NULL
)engine = InnoDB;

CREATE TABLE if not exists compras
(
id INTEGER(11) PRIMARY KEY AUTO_INCREMENT,
comprobante VARCHAR(15) NOT NULL,
num_comprobante INTEGER(11) NOT NULL,
descripcion VARCHAR(25) NOT NULL,
fecha DATE NOT NULL,
id_proveedor INTEGER(11),
id_usuario VARCHAR(10),
FOREIGN KEY (id_proveedor) REFERENCES proveedores(id),
FOREIGN KEY (id_usuario) REFERENCES usuarios(id)
)engine = InnoDB;

CREATE TABLE if not exists detalle_compras
(
id INTEGER(11) PRIMARY KEY AUTO_INCREMENT,
/*stock INTEGER(11) NOT NULL*/
cantidad INTEGER(11) NOT NULL,
/**medida VARCHAR(10) NOT NULL,*/
precio DECIMAL(10,2) NOT NULL,
id_articulo INTEGER(11),
id_compra INTEGER(11),
FOREIGN KEY (id_articulo) REFERENCES articulos(id),
FOREIGN KEY (id_compra)	REFERENCES compras(id)
)engine = InnoDB;


CREATE OR REPLACE VIEW  vista_productos 
AS SELECT 
productos.codigo,
productos.nombre,
productos.precio,
categorias.nombre AS nombre_categoria
FROM productos,categorias where productos.id_categoria = categorias.id;

CREATE OR REPLACE VIEW vista_detalle_productos 
AS SELECT 
detalle_productos.id,
productos.codigo, 
productos.nombre, 
articulos.nombre AS nombre_articulo, 
CONCAT(detalle_productos.cantidad,' - ',articulos.medida) AS cantidad
FROM productos, articulos, detalle_productos
WHERE (productos.id = detalle_productos.id_producto AND articulos.id = detalle_productos.id_articulo) ;

CREATE OR REPLACE VIEW vista_clientes AS
SELECT * FROM cliente